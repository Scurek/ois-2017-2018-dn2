/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://teaching.lavbic.net/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  };
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://teaching.lavbic.net/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://teaching.lavbic.net/OIS/gradivo/") > -1;
  var jeyt = sporocilo.indexOf("https://www.youtube.com/watch?v=") > -1;
  if (jeSmesko && jeyt) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />")
               .split("&lt;iframe").join("<iframe")
               .split("allowfullscreen&gt;").join("allowfullscreen>")
               .split("&lt;/iframe&gt;").join("</iframe>");
    var chopchop = sporocilo.indexOf("<iframe");
    sporocilo = [sporocilo.slice(0, chopchop), " <br> ", sporocilo.slice(chopchop)].join('');
    //console.log (sporocilo);
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  } 
  else if (jeSmesko) {
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } 
  else if (jeyt) {
    //return divElementHtmlTekst(sporocilo);
    sporocilo =
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;iframe").join("<iframe")
               .split("allowfullscreen&gt;").join("allowfullscreen>")
               .split("&lt;/iframe&gt;").join("</iframe>");
    var chopchop = sporocilo.indexOf("<iframe");
    sporocilo = [sporocilo.slice(0, chopchop), " <br> ", sporocilo.slice(chopchop)].join('');
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  }
  else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}



/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
 
 
function sprocesirajYtVidejeProsim(vhodnoBesedilo) {
  
  if (vhodnoBesedilo.indexOf("https://www.youtube.com/watch?v=") > -1){
    //vhodnoBesedilo = vhodnoBesedilo.concat("<br>");
    var parametri = vhodnoBesedilo.split(' ');
    for (var i = 0; i<parametri.length; i++){
      if (parametri[i].indexOf("https://www.youtube.com/watch?v=") > -1){
        var maliparametri = parametri[i].split("https://www.youtube.com/watch?v=");
        if (vhodnoBesedilo.indexOf("/zasebno") == 0){
          maliparametri[1] = maliparametri[1].substr(0,maliparametri[1].length-1);
          vhodnoBesedilo = vhodnoBesedilo.substr(0,vhodnoBesedilo.length-1);
          //console.log (vhodnoBesedilo);
          vhodnoBesedilo = vhodnoBesedilo.concat("<iframe class='linki' src='https://www.youtube.com/embed/" + maliparametri[1] + "' allowfullscreen></iframe>\"");
          console.log (vhodnoBesedilo);
        }
        else {
          vhodnoBesedilo = vhodnoBesedilo.concat("<iframe class='linki' src='https://www.youtube.com/embed/" + maliparametri[1] + "' allowfullscreen></iframe>");
        }
      }
    }
  }
  return vhodnoBesedilo;
}

function ajmoPovejMiKduJeBilOmenjen(klepetApp, sporocilo) {
  
  //console.log (elementi);
  if (sporocilo.indexOf("/zasebno") == 0){
    var elementi = sporocilo.split("\"");
    elementi = elementi[3].split(" ");
  }
  else {
    var elementi = sporocilo.split(" ");
  }
  console.log (elementi);
  for (var i = 0;i<elementi.length; i++){
    if (elementi[i].charAt(0)=='@'){
      var vzdevek = elementi[i].substring(1, elementi[i].length);
      if (vzdevek != trenutniVzdevek){
        //console.log ("Nasel omembo: "+vzdevek);
        klepetApp.posljiVzdevek(vzdevek);
      }
    }
  }
}
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = sprocesirajYtVidejeProsim(sporocilo);
  var sistemskoSporocilo;
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.substring(0, 6).toLowerCase() == '/barva') {
    var parametri = sporocilo.split(' ');
    document.getElementById("sporocila").style.color = parametri[1];
    document.getElementById("kanal").style.color = parametri[1];
  }
  else if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      if (sistemskoSporocilo.indexOf("https://www.youtube.com/watch?v=") > -1) {
        var chopchop = sistemskoSporocilo.indexOf("<iframe");
        sistemskoSporocilo = [sistemskoSporocilo.slice(0, chopchop), " <br> ", sistemskoSporocilo.slice(chopchop)].join('');
      }
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtrirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    console.log (sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
  $('#poslji-sporocilo').val('');
  ajmoPovejMiKduJeBilOmenjen(klepetApp, sporocilo);
}


// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('./swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n');
});


/**
 * Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj 
 * z enako dolžino zvezdic (*)
 * 
 * @param vhodni niz
 */
function filtrirajVulgarneBesede(vhod) {
  for (var i in vulgarneBesede) {
    var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
    vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
  }
  return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";


// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    //var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    if (sporocilo.omemba){
      var novElement = divElementHtmlTekst(sporocilo.omemba+" (zasebno): &#9758; Omemba v klepetu");
      $('#sporocila').append(novElement);
    }
    else if (sporocilo.neomemba){
      var novElement = divElementHtmlTekst(sporocilo.besedilo);
      $('#sporocila').append(novElement);
    }
    else{
      var novElement = divElementEnostavniTekst(sporocilo.besedilo);
      $('#sporocila').append(novElement);
    }
    $('#sporocila').append(novElement);
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  socket.on('uporabniki', function(uporabniki) {
    $("#seznam-uporabnikov").empty();
    for (var i=0; i < uporabniki.length; i++) {
      $("#seznam-uporabnikov").append(divElementEnostavniTekst(uporabniki[i]));
    }
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});


/* global $, io, Klepet */